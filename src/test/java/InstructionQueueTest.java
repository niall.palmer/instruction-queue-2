import instruction.queue.InstructionMessage;
import instruction.queue.InstructionQueue;

import instruction.queue.InstructionType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.PriorityQueue;
import java.util.Queue;

import static org.junit.jupiter.api.Assertions.*;

class InstructionQueueTest {
    InstructionMessage instructionMessage;
    InstructionQueue instructionQueue;

    @BeforeEach
    public void setup() {
        instructionMessage = new InstructionMessageTestBuilder().build();
        instructionQueue = new InstructionQueue();
    }

    @Test
    void enqueueCorrectlyPutsInstructionMessageIntoQueue() {
        instructionQueue.enqueue(instructionMessage);
        Queue<InstructionMessage> instructionMessageAsList = new PriorityQueue<>();
        instructionMessageAsList.add(instructionMessage);
        assertEquals(instructionMessageAsList.peek(), instructionQueue.peek());
    }
    
    @Test
    void receivePutsValidInstructionMessageIntoQueue() {
        String instructionMessageAsString = "InstructionMessage A AB12 10 128 2023-06-01T09:30:30.456";
        instructionQueue.receive(instructionMessageAsString);
    
        Queue<InstructionMessage> instructionMessageAsList = new PriorityQueue<>();
        instructionMessageAsList.add(instructionMessage);
        assertEquals(instructionMessageAsList.peek(), instructionQueue.peek());
    }
    
    @Test
    void receiveThrowsExceptionWhenStringHasTooManyArguments() {
        String instructionMessageAsStringTooManyArguments = "InstructionMessage A AB12 10 128 2023-06-01T09:30:30.456 XYZABC123";
        assertThrows(IllegalArgumentException.class, () -> instructionQueue.receive(instructionMessageAsStringTooManyArguments));
    }
    
    @Test
    void receiveThrowsExceptionWhenStringHasNotEnoughArguments() {
        String instructionMessageAsStringNotEnoughArguments = "InstructionMessage A AB12 10 128";
        assertThrows(IllegalArgumentException.class, () -> instructionQueue.receive(instructionMessageAsStringNotEnoughArguments));
    }
    
    @Test
    void instructionTypeAShouldHaveHigherPriorityThanB() {
        InstructionQueue instructionQueue2 = new InstructionQueue();
        InstructionMessage instructionMessageInstructionTypeA = new InstructionMessageTestBuilder().withInstructionType(InstructionType.B).build();
        InstructionMessage instructionMessageInstructionTypeB = new InstructionMessageTestBuilder().withInstructionType(InstructionType.C).build();
        
        instructionQueue.enqueue(instructionMessageInstructionTypeA);
        instructionQueue.enqueue(instructionMessageInstructionTypeB);
        assertEquals(instructionMessageInstructionTypeA, instructionQueue.peek());
        
        instructionQueue2.enqueue(instructionMessageInstructionTypeB);
        instructionQueue2.enqueue(instructionMessageInstructionTypeA);
        assertEquals(instructionMessageInstructionTypeA, instructionQueue2.peek());
    }
    
    @Test
    void instructionTypeBShouldBeHigherPriorityThanC() {
        InstructionQueue instructionQueue2 = new InstructionQueue();
        InstructionMessage instructionMessageInstructionTypeB = new InstructionMessageTestBuilder().withInstructionType(InstructionType.B).build();
        InstructionMessage instructionMessageInstructionTypeC = new InstructionMessageTestBuilder().withInstructionType(InstructionType.C).build();
    
        instructionQueue.enqueue(instructionMessageInstructionTypeB);
        instructionQueue.enqueue(instructionMessageInstructionTypeC);
        assertEquals(instructionMessageInstructionTypeB, instructionQueue.peek());
    
        instructionQueue2.enqueue(instructionMessageInstructionTypeC);
        instructionQueue2.enqueue(instructionMessageInstructionTypeB);
        assertEquals(instructionMessageInstructionTypeB, instructionQueue2.peek());
    }
    
    @Test
    void instructionTypesCandDHaveSamePriority() {
        InstructionQueue instructionQueue2 = new InstructionQueue();
        InstructionMessage instructionMessageInstructionTypeC = new InstructionMessageTestBuilder().withInstructionType(InstructionType.C).build();
        InstructionMessage instructionMessageInstructionTypeD = new InstructionMessageTestBuilder().withInstructionType(InstructionType.D).build();
    
        instructionQueue.enqueue(instructionMessageInstructionTypeC);
        instructionQueue.enqueue(instructionMessageInstructionTypeD);
        assertEquals(instructionMessageInstructionTypeC, instructionQueue.peek());
    
        instructionQueue2.enqueue(instructionMessageInstructionTypeD);
        instructionQueue2.enqueue(instructionMessageInstructionTypeC);
        assertEquals(instructionMessageInstructionTypeD, instructionQueue2.peek());
    }
    
    @Test
    void dequeueCorrectlyRemovesInstructionMessageFromQueueAndReturnsIt() {
        instructionQueue.enqueue(instructionMessage);
        assertEquals(instructionMessage, instructionQueue.dequeue());
    }
    
    @Test
    void countReturnsZeroWhenNoMessagesOnQueue() {
        assertEquals(0, instructionQueue.count());
    }
    
    @Test
    void countReturnsOneWhenMessageAddedToQueue() {
        instructionQueue.enqueue(instructionMessage);
        assertEquals(1, instructionQueue.count());
    }
    
    @Test
    void isEmptyReturnsTrueWhenQueueIsEmpty() {
        assertTrue(instructionQueue.isEmpty());
    }
    
    @Test
    void isEmptyReturnsFalseWhenQueueIsNotEmpty() {
        instructionQueue.enqueue(instructionMessage);
        assertFalse(instructionQueue.isEmpty());
    }
}