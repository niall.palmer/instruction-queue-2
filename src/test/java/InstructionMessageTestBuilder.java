import instruction.queue.InstructionMessage;
import instruction.queue.InstructionType;

import static instruction.queue.InstructionType.A;

public class InstructionMessageTestBuilder {
    private String instructionMessageStr = "InstructionMessage";
    private InstructionType instructionType = A;
    private String productCode = "AB12";
    private int quantity = 10;
    private int uom = 128;
    private String timestamp = "2023-06-01T09:30:30.456";

    public InstructionMessageTestBuilder withInstructionMessageStr(String instructionMessageStr) {
        this.instructionMessageStr = instructionMessageStr;
        return this;
    }

    public InstructionMessage build() {
        return new InstructionMessage(instructionMessageStr, instructionType, productCode, quantity, uom, timestamp);
    }

    public InstructionMessageTestBuilder withProductCode(String productCode) {
        this.productCode = productCode;
        return this;
    }

    public InstructionMessageTestBuilder withQuantity(int quantity) {
        this.quantity = quantity;
        return this;
    }

    public InstructionMessageTestBuilder withUom(int uom) {
        this.uom = uom;
        return this;
    }
    
    public InstructionMessageTestBuilder withTimestamp(String timestamp) {
        this.timestamp = timestamp;
        return this;
    }
    
    public InstructionMessageTestBuilder withInstructionType(InstructionType instructionType) {
        this.instructionType = instructionType;
        return this;
    }
}
