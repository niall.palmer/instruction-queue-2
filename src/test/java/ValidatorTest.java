import instruction.queue.InstructionMessage;
import instruction.queue.InstructionQueue;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;

public class ValidatorTest {
    
    InstructionMessage instructionMessage;
    InstructionMessage incorrectInstructionMessage;
    InstructionQueue instructionQueue;
    
    @BeforeEach
    public void setup() {
        instructionMessage = new InstructionMessageTestBuilder().build();
        instructionQueue = new InstructionQueue();
    }
    
    @Test
    void exceptionThrownWhenInstructionMessageStrMisspelled() {
        String instructionMessageStrMisspelled = "InsturctionMessagi";
        incorrectInstructionMessage = new InstructionMessageTestBuilder().withInstructionMessageStr(instructionMessageStrMisspelled).build();
        assertThrows(IllegalArgumentException.class, () -> instructionQueue.enqueue(incorrectInstructionMessage));
    }
    
    @Test
    void exceptionThrownWhenProductCodeHasTooManyCharacters() {
        String productCodeTooManyCharacters = "AB123";
        incorrectInstructionMessage = new InstructionMessageTestBuilder().withProductCode(productCodeTooManyCharacters).build();
        assertThrows(IllegalArgumentException.class, () -> instructionQueue.enqueue(incorrectInstructionMessage));
    }
    
    @Test
    void exceptionThrownWhenProductCodeHasNotEnoughCharacters() {
        String productCodeNotEnoughCharacters = "AB1";
        incorrectInstructionMessage = new InstructionMessageTestBuilder().withProductCode(productCodeNotEnoughCharacters).build();
        assertThrows(IllegalArgumentException.class, () -> instructionQueue.enqueue(incorrectInstructionMessage));
    }
    
    @Test
    void exceptionThrownWhenProductCodeHasNumbersBeforeLetters() {
        String productCodeNumbersBeforeLetters = "12AB";
        incorrectInstructionMessage = new InstructionMessageTestBuilder().withProductCode(productCodeNumbersBeforeLetters).build();
        assertThrows(IllegalArgumentException.class, () -> instructionQueue.enqueue(incorrectInstructionMessage));
    }
    
    @Test
    void exceptionThrownWhenQuantityBelowOne() {
        int zeroQuantity = 0;
        incorrectInstructionMessage = new InstructionMessageTestBuilder().withQuantity(zeroQuantity).build();
        assertThrows(IllegalArgumentException.class, () -> instructionQueue.enqueue(incorrectInstructionMessage));
    }
    
    @Test
    void exceptionThrownWhenUomBelowZero() {
        int uomBelowZero = -1;
        incorrectInstructionMessage = new InstructionMessageTestBuilder().withUom(uomBelowZero).build();
        assertThrows(IllegalArgumentException.class, () -> instructionQueue.enqueue(incorrectInstructionMessage));
    }
    
    @Test
    void exceptionThrownWhenUomAbove255() {
        int uomAbove255 = 256;
        incorrectInstructionMessage = new InstructionMessageTestBuilder().withUom(uomAbove255).build();
        assertThrows(IllegalArgumentException.class, () -> instructionQueue.enqueue(incorrectInstructionMessage));
    }
    
    @Test
    void exceptionThrownWhenTimestampHasNoMilliseconds() {
        String timestampWithoutMilliseconds = "2023-06-01T09:30:30";
        incorrectInstructionMessage = new InstructionMessageTestBuilder().withTimestamp(timestampWithoutMilliseconds).build();
        assertThrows(IllegalArgumentException.class, () -> instructionQueue.enqueue(incorrectInstructionMessage));
    }
    
    @Test
    void exceptionThrownWhenTimestampHasIncorrectDelimiter() {
        String timestampIncorrectDelimiter = "2023-06-01X09:30:30.456";
        incorrectInstructionMessage = new InstructionMessageTestBuilder().withTimestamp(timestampIncorrectDelimiter).build();
        assertThrows(IllegalArgumentException.class, () -> instructionQueue.enqueue(incorrectInstructionMessage));
    }
    
    @Test
    void exceptionThrownWhenTimestampBeforeUnixEpoch() {
        String timestampBeforeUnixEpoch = "1969-12-31T23:59:59.999";
        incorrectInstructionMessage = new InstructionMessageTestBuilder().withTimestamp(timestampBeforeUnixEpoch).build();
        assertThrows(IllegalArgumentException.class, () -> instructionQueue.enqueue(incorrectInstructionMessage));
    }
    
    @Test
    void exceptionThrownWhenTimestampAfterCurrentTime() {
        String timestampAfterCurrentTime = "2029-01-15T09:30:30.123";
        incorrectInstructionMessage = new InstructionMessageTestBuilder().withTimestamp(timestampAfterCurrentTime).build();
        assertThrows(IllegalArgumentException.class, () -> instructionQueue.enqueue(incorrectInstructionMessage));
    }
}
