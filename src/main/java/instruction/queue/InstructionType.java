package instruction.queue;

public enum InstructionType {
    A(0),
    B(1),
    C(2),
    D(2);
    
    public final int priority;
    
    InstructionType(int priority) {
        this.priority = priority;
    }
    
    public int getPriority() {
        return priority;
    }
}
