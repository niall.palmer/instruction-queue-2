package instruction.queue;

import instruction.queue.validators.*;

import java.util.ArrayList;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Queue;

public class InstructionQueue implements MessageReceiver {
    private final Queue<InstructionMessage> instructionQueue = new PriorityQueue<>();
    private final List<Validator> validators = new ArrayList<>();

    public void applyValidators() {
        validators.add(new InstructionMessageStrValidator());
        validators.add(new ProductCodeValidator());
        validators.add(new QuantityValidator());
        validators.add(new UomValidator());
        validators.add(new TimestampValidator());
    }

    public void enqueue(InstructionMessage instructionMessage) {
        applyValidators();

        for (Validator validator : validators) {
            validator.checkValidation(instructionMessage);
        }

        instructionQueue.add(instructionMessage);
    }

    @Override
    public void receive(String message) {
        List<String> splitMessage = List.of(message.split(" "));
        if (splitMessage.size() != 6)
            throw new IllegalArgumentException("The message must contain exactly 6 arguments");
        
        InstructionMessage instructionMessage = new InstructionMessage(
                splitMessage.get(0),
                InstructionType.valueOf(splitMessage.get(1)),
                splitMessage.get(2),
                Integer.parseInt(splitMessage.get(3)),
                Integer.parseInt(splitMessage.get(4)),
                splitMessage.get(5)
        );
        enqueue(instructionMessage);
    }
    
    public InstructionMessage peek() {
        return instructionQueue.peek();
    }
    
    public InstructionMessage dequeue() {
        return instructionQueue.poll();
    }
    
    public int count() {
        return instructionQueue.size();
    }
    
    public boolean isEmpty() {
        return instructionQueue.isEmpty();
    }
}
