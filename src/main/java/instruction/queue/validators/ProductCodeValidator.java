package instruction.queue.validators;

import instruction.queue.InstructionMessage;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ProductCodeValidator implements Validator {

    @Override
    public void checkValidation(InstructionMessage instructionMessage) {
        Pattern pattern = Pattern.compile("[A-Z]{2}[0-9]{2}");
        Matcher matcher = pattern.matcher(instructionMessage.productCode());
        if (!matcher.matches())
            throw new IllegalArgumentException("The product code must be exactly four characters long, and be in the format AB12");
    }
}
