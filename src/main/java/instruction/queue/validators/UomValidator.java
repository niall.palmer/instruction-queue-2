package instruction.queue.validators;

import instruction.queue.InstructionMessage;

public class UomValidator implements Validator {
    @Override
    public void checkValidation(InstructionMessage instructionMessage) {
        if (instructionMessage.uom() < 0 || instructionMessage.uom() > 255) {
            throw new IllegalArgumentException("The UOM should be between 0 and 255 inclusive");
        }
    }
}
