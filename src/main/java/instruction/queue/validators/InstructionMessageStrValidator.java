package instruction.queue.validators;

import instruction.queue.InstructionMessage;

public class InstructionMessageStrValidator implements Validator {
    @Override
    public void checkValidation(InstructionMessage instructionMessage) {
        if (!instructionMessage.instructionMessageStr().equals("InstructionMessage"))
            throw new IllegalArgumentException("The instruction message must begin with: InstructionMessage");
    }
}
