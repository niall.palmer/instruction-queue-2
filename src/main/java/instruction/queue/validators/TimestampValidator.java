package instruction.queue.validators;

import instruction.queue.InstructionMessage;

import java.time.DateTimeException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class TimestampValidator implements Validator {
    @Override
    public void checkValidation(InstructionMessage instructionMessage) {
        LocalDateTime timestamp;
        LocalDateTime currentDateTime = LocalDateTime.now();
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("uuuu-MM-dd'T'HH:mm:ss.SSS");
        LocalDateTime unixEpoch = LocalDateTime.parse("1970-01-01T00:00:00.000");
        
        try {
            timestamp = LocalDateTime.parse(instructionMessage.timestamp(), dateTimeFormatter);
        } catch (DateTimeException e) {
            throw new IllegalArgumentException("The datetime isn't in the correct format");
        }
        timestamp = LocalDateTime.parse(instructionMessage.timestamp(), dateTimeFormatter);
        if (timestamp.isBefore(unixEpoch))
            throw new IllegalArgumentException("The datetime is before unix epoch");
        if (timestamp.isAfter(currentDateTime))
            throw new IllegalArgumentException("The datetime is after now");
    }
}
