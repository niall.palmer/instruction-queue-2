package instruction.queue.validators;

import instruction.queue.InstructionMessage;

public interface Validator {
    public void checkValidation(InstructionMessage instructionMessage);
}
