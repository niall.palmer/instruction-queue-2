package instruction.queue.validators;

import instruction.queue.InstructionMessage;

public class QuantityValidator implements Validator {
    @Override
    public void checkValidation(InstructionMessage instructionMessage) {
        if (instructionMessage.quantity() < 1) {
            throw new IllegalArgumentException("The quantity must be above 0");
        }
    }
}
