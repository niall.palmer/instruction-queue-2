package instruction.queue;

import java.util.Comparator;
import java.util.Objects;

public record InstructionMessage(String instructionMessageStr, InstructionType instructionType, String productCode,
                                 int quantity, int uom, String timestamp) implements Comparable<InstructionMessage> {
    
    @Override
    public boolean equals(Object obj) {
        if (obj == this) return true;
        if (obj == null || obj.getClass() != this.getClass()) return false;
        var that = (InstructionMessage) obj;
        return Objects.equals(this.instructionMessageStr, that.instructionMessageStr) &&
                Objects.equals(this.instructionType, that.instructionType) &&
                Objects.equals(this.productCode, that.productCode) &&
                this.quantity == that.quantity &&
                this.uom == that.uom &&
                Objects.equals(this.timestamp, that.timestamp);
    }
    
    @Override
    public String toString() {
        return "InstructionMessage[" +
                "instructionMessageStr=" + instructionMessageStr + ", " +
                "instructionType=" + instructionType + ", " +
                "productCode=" + productCode + ", " +
                "quantity=" + quantity + ", " +
                "uom=" + uom + ", " +
                "timestamp=" + timestamp + ']';
    }
    
    @Override
    public int compareTo(InstructionMessage instructionMessage) {
        return Integer.compare(this.instructionType().getPriority(), instructionMessage.instructionType().getPriority());
    }
}
